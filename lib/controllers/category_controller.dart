import 'package:get/get.dart';

class CategoryController extends GetxController {
  int _selectedCategoryIndex = 0;

  int get selectedIndex => _selectedCategoryIndex;

  set selectedIndex(int value) {
    _selectedCategoryIndex = value;
    update();
  }
}
