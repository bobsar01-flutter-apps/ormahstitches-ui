import 'package:get/get.dart';

class CartController extends GetxController {
  RxInt numOfItems = 1.obs;

  void incrementCart() => numOfItems++;
  void decrementCart() {
    if (numOfItems > 1) {
      numOfItems--;
    }
  }
}
