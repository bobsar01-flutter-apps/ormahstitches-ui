import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_shop_app/constants.dart';
import 'package:online_shop_app/controllers/cart_controller.dart';
import 'package:online_shop_app/controllers/category_controller.dart';
import 'package:online_shop_app/screens/home/home_screen.dart';

void main() {
  initialize();
  runApp(MyApp());
}

void initialize() {
  Get.put(CategoryController());
  Get.put(CartController());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    );
  }
}
