import 'package:flutter/material.dart';
import 'package:online_shop_app/constants.dart';
import 'package:online_shop_app/models/Product.dart';
import 'package:online_shop_app/screens/details/components/details_body.dart';

class DetailsScreen extends StatelessWidget {
  final Product product;

  const DetailsScreen({Key? key, required this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: product.color,
      appBar: buildAppBar(),
      body: DetailsBody(product),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: product.color,
      elevation: 0,
      // leading: IconButton(
      //     icon: SvgPicture.asset("assets/icons/back.svg"),
      //     onPressed: () => Navigator.pop(context)),
      actions: <Widget>[
        IconButton(
            onPressed: () {},
            icon: Icon(Icons.search, color: Colors.white),
            color: kTextColor),
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.shopping_cart_outlined,
              color: Colors.white,
            ),
            color: kTextColor),
        SizedBox(
          width: kDefaultPadding / 2,
        )
      ],
    );
  }
}
