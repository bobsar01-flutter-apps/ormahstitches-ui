import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_shop_app/constants.dart';
import 'package:online_shop_app/controllers/category_controller.dart';

class Categories extends StatelessWidget {
  final CategoryController _categoryController = Get.put(CategoryController());

  final List<String> categories = [
    "Hand bag",
    "Jewellery",
    'Footwear',
    "Casual wears",
    "Make Ups"
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 25,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: categories.length,
          itemBuilder: (context, index) => buildCategory(index)),
    );
  }

  Widget buildCategory(int index) {
    return GestureDetector(
      onTap: () {
        print('tapped!!!!!');
        _categoryController.selectedIndex = index;
        // setState(() {
        //   selectedIndex = index;
        // });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
        child: GetBuilder<CategoryController>(
          builder: (controller) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  categories[index],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: controller.selectedIndex == index
                        ? kTextColor
                        : kTextLightColor,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: kDefaultPadding / 4),
                  //top padding 5
                  height: 2,
                  width: 30,
                  color: controller.selectedIndex == index
                      ? Colors.black
                      : Colors.transparent,
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
