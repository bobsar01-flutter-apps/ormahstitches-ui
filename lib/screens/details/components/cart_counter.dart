import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_shop_app/constants.dart';
import 'package:online_shop_app/controllers/cart_controller.dart';

class CartCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetX<CartController>(
      // initState: (_) {},
      builder: (controller) {
        return Row(
          children: <Widget>[
            buildOutlineButton(Icons.remove, controller.decrementCart),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
              child: Text(
                // if less than 10, prefix with "0"
                controller.numOfItems.toString().padLeft(2, "0"),
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            buildOutlineButton(Icons.add, controller.incrementCart),
          ],
        );
      },
    );
  }

  SizedBox buildOutlineButton(IconData icon, Function() press) {
    return SizedBox(
      width: 40,
      height: 32,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
            padding: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(13))),
        onPressed: press,
        child: Icon(icon),
      ),
    );
  }
}
